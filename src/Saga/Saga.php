<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

use GDXbsv\PServiceBus\Message\EventOptions;
use GDXbsv\PServiceBus\Message\Message;
use GDXbsv\PServiceBus\Message\TimeSpan;

abstract class Saga
{
    /** @var int */
    protected int $_playhead = 0;
    /**
     * @psalm-readonly-allow-private-mutation
     */
    public ?\DateTimeImmutable $completedAt = null;

    abstract public static function configureHowToCreateSaga(SagaCreateMapper $mapper): void;

    abstract public static function configureHowToFindSaga(SagaPropertyMapper $mapper): void;

    protected function markAsComplete(): void
    {
        $this->completedAt = new \DateTimeImmutable();
    }
}
