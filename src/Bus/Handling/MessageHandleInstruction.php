<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Bus\Handling;

use GDXbsv\PServiceBus\Message\TimeSpan;

/**
 * @immutable
 * @psalm-immutable
 */
final readonly class MessageHandleInstruction
{
    public function __construct(
        public string $messageClass,
        public string $transportName,
        public ?int $retries,
        public ?TimeSpan $timeout,
        public ?string $handlerName,
        public string $handlerMethodName,
        public bool $isStaticMethod,
        public ?string $retriesTimeoutExpression,
    ) {
    }

    /**
     * @param array{messageClass: string, transportName: string, retries: ?int, timeout: ?TimeSpan, handlerName: ?string, handlerMethodName: string, isStaticMethod: bool, retriesTimeoutExpression: ?string} $data
     * @return static
     */
    public static function __set_state(array $data): self
    {
        return new self(
            $data['messageClass'],
            $data['transportName'],
            $data['retries'],
            $data['timeout'],
            $data['handlerName'],
            $data['handlerMethodName'],
            $data['isStaticMethod'],
            $data['retriesTimeoutExpression'],
        );
    }
}
