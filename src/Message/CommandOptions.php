<?php
declare(strict_types=1);

namespace GDXbsv\PServiceBus\Message;

/**
 * @immutable
 * @psalm-immutable
 */
final class CommandOptions extends MessageOptions
{
    protected static string $messageType = 'command';
}
