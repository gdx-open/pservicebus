<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBusTests\Unit\Message;

use GDXbsv\PServiceBus\Message\MessageOptions;
use GDXbsv\PServiceBus\Message\TimeSpan;
use JetBrains\PhpStorm\Immutable;
use PHPUnit\Framework\TestCase;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 */
#[Immutable]
final class MessageOptionsTest extends TestCase
{
    public function testKeepValues()
    {
        $optionsOrig = MessageOptions::record(['sdf' => 89]);
        $options = clone $optionsOrig;

        $options = $options->withTimeout(TimeSpan::fromSeconds(3456));
        $options = $options->withRetries(46);
        $options = $options->withRetriesTimeoutExpression('34 * (67 ** retries_count)');
        $options = $options
            ->withHeader('saga', 'name')
            ->withHeader('type', 'saga');

        self::assertSame(89, $options->headers['sdf']);
        self::assertSame('name', $options->headers['saga']);
        self::assertSame('saga', $options->headers['type']);
        self::assertSame(TimeSpan::fromSeconds(3456)->intervalSec, $options->timeout->intervalSec);
        self::assertSame(46, $options->retries);
        self::assertSame('34 * (67 ** retries_count)', $options->retriesTimeoutExpression);
    }
}
