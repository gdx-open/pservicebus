<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Message\Replay;

use GDXbsv\PServiceBus\Bus\CoroutineBus;
use GDXbsv\PServiceBus\Message\EventOptions;
use GDXbsv\PServiceBus\Message\Message;

/**
 * @psalm-type ReplayOutput = \Traversable<Message<EventOptions>>
 */
class Replaying
{
    /**
     * Replaying constructor.
     * @param array<non-empty-string, ReplayInstruction> $replays
     * @param array<non-empty-string, object> $replayToObjectMap
     */
    public function __construct(
        private CoroutineBus $coroutineBus,
        private array $replays,
        private array $replayToObjectMap
    ) {
    }

    /**
     * @param non-empty-string $replayName
     * @param non-empty-string $handler
     * @param non-empty-string $transportName
     * @return \Traversable<int, int>
     */
    public function __invoke(
        string $replayName,
        string $handler,
        string $transportName,
        bool $external = false
    ): \Traversable {
        $publisher = $this->coroutineBus->publishCoroutine();
        if (!isset($this->replays[$replayName])) {
            throw new \RuntimeException("Replay with name '$replayName' does not exist.");
        }
        $replayInstruction = $this->replays[$replayName];
        if (!isset($this->replayToObjectMap[$replayInstruction->className])) {
            throw new \RuntimeException("Can not find object for '$replayInstruction->className'.");
        }
        $object = $this->replayToObjectMap[$replayInstruction->className];
        /**
         * @var Message<EventOptions> $message
         * @psalm-suppress MixedMethodCall yes we do not know the object type
         */
        foreach ($object->{$replayInstruction->methodName}() as $message) {
            if (!$external) {
                /** @psalm-suppress PossiblyUndefinedIntArrayOffset we are sure that handler has :: */
                [$handlerName, $handlerMethodName] = explode('::', $handler);
                $publisher->publish(
                    $message->payload,
                    $message->options
                        ->withHeader('message_type', 'internal')
                        ->withHeader('replay_type', 'internal')
                        ->withHeader('handlerName', $handlerName)
                        ->withHeader('handlerMethodName', $handlerMethodName)
                        ->withHeader('transportName', $transportName)
                );
            } else {
                $publisher->publish(
                    $message->payload,
                    $message->options
                        ->withHeader('message_type', 'external')
                        ->withHeader('replay_type', 'external')
                        ->withHeader('transportName', $transportName)
                );
            }

            yield 1;
        }
        $publisher->finish();
    }
}
