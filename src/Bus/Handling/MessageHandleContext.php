<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Bus\Handling;

use GDXbsv\PServiceBus\Bus;
use GDXbsv\PServiceBus\Bus\CoroutineBus;
use GDXbsv\PServiceBus\Bus\CoroutineSending\CommandCoroutineSender;
use GDXbsv\PServiceBus\Bus\CoroutineSending\EventCoroutineSender;
use GDXbsv\PServiceBus\Message\CommandOptions;
use GDXbsv\PServiceBus\Message\EventOptions;
use GDXbsv\PServiceBus\Message\MessageOptions;

/**
 * @psalm-suppress MissingImmutableAnnotation Will remove it in the next version
 */
final class MessageHandleContext implements Bus, CoroutineBus
{
    public function __construct(
        public MessageOptions $messageOptions,
        private Bus $bus,
        private CoroutineBus $coroutineBus,
    ) {
    }

    public function send(object $message, ?CommandOptions $commandOptions = null): void
    {
        $this->bus->send($message, $commandOptions);
    }

    public function publish(object $message, ?EventOptions $eventOptions = null): void
    {
        $this->bus->publish($message, $eventOptions);
    }

    public function sendCoroutine(): CommandCoroutineSender
    {
        return $this->coroutineBus->sendCoroutine();
    }

    public function publishCoroutine(): EventCoroutineSender
    {
        return $this->coroutineBus->publishCoroutine();
    }
}
