<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBusTests\Integration\Handling;

use GDXbsv\PServiceBus\Bus;
use GDXbsv\PServiceBusTestApp\Handling\MultiTransportMessage;
use GDXbsv\PServiceBusTestApp\InMemoryTraceTransport;
use GDXbsv\PServiceBusTests\Integration\IntegrationTestCase;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 */
final class MultiTransportsTest extends IntegrationTestCase
{
    public function testMulti(): void
    {
        $bus = $this->bus;
        $bus->publish(new MultiTransportMessage());
        $transport1 = $this->inMemTraceTransport1;
        $transport2 = $this->inMemTraceTransport2;
        $this->assertEquals('1', $transport1->name);
        $this->assertEquals('2', $transport2->name);
        $this->assertCount(1, $transport1->envelopesRecorded);
        $this->assertCount(1, $transport2->envelopesRecorded);

    }
}
