<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

/**
 * @immutable
 * @psalm-immutable
 */
abstract class SagaCreateToMessageMap
{
    /** @var class-string<Saga> */
    protected string $sagaType;

    abstract function createSagaCreatorDefinition(): SagaCreatorDefinition;
}
