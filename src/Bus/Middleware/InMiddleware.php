<?php
declare(strict_types=1);

namespace GDXbsv\PServiceBus\Bus\Middleware;

use GDXbsv\PServiceBus\Message\Message;
use Prewk\Result;

interface InMiddleware
{
    public function before(Message $message): void;
    public function after(Result $result): void;
}
