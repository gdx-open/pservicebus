<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Message\Replay;

/**
 * @immutable
 * @psalm-immutable
 */
#[\Attribute(\Attribute::TARGET_FUNCTION | \Attribute::TARGET_METHOD)]
final class Replay
{
    /**
     * @param non-empty-string $replayName
     */
    public function __construct(
        /** @var non-empty-string */
        public string $replayName,
    ) {
    }
}
