<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

use GDXbsv\PServiceBus\Message\MessageOptions;

/**
 * @psalm-suppress MissingImmutableAnnotation Will remove it in the next version
 */
final class MessageSagaContext
{
    public function __construct(
        public MessageOptions $messageOptions,
    ) {
    }
}
