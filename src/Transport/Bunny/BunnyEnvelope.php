<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Transport\Bunny;

use Bunny\Message;
use GDXbsv\PServiceBus\Bus\Handling\Handle;
use GDXbsv\PServiceBus\Transport\Envelope;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 */
final class BunnyEnvelope
{
    /**
     * @param array<string, string|int|bool|null> $headers
     */
    public function __construct(
        private string $payload,
        private int $retries,
        private string $retriesTimeoutExpression,
        private int $timeoutSec,
        /** @var array<string, string|int|bool|null> */
        private array $headers,
    ) {
    }

    public static function ofEnvelope(Envelope $envelope): self
    {
        return new self(
            \json_encode($envelope->payload, JSON_THROW_ON_ERROR),
            $envelope->retries,
            $envelope->retriesTimeoutExpression,
            $envelope->timeoutSec,
            $envelope->headers
        );
    }

    public static function ofBunnyMessage(Message $message): self
    {
        $reties = $message->headers['retries'] ?? 0;
        $timeoutSec = $message->headers['timeout_sec'] ?? 0;
        $retriesTimeoutExpression = $message->headers['retries_timeout_expression'] ?? (new Handle(''))->retriesTimeoutExpression;
        /** @psalm-suppress MixedArgumentTypeCoercion */
        return new self($message->content, $reties, $retriesTimeoutExpression, $timeoutSec, $message->headers);
    }

    public function toBunnyPublish(): array
    {
        $headers = $this->headers;
        $headers['retries'] = $this->retries;
        $headers['retries_timeout_expression'] = $this->retriesTimeoutExpression;
        $headers['timeout_sec'] = $this->timeoutSec;
        $headers['psb-retried'] = 0;
        if ($this->timeoutSec > 0) {
            $headers['expiration'] = $this->timeoutSec * 1000;
        }

        return [$this->payload, $headers];
    }

    public function toEnvelope(): Envelope
    {
        /** @psalm-suppress MixedArgument we control what we receive. It must be an array */
        return new Envelope(
            \json_decode($this->payload, true), $this->retries, $this->retriesTimeoutExpression, $this->timeoutSec, $this->headers
        );
    }
}
