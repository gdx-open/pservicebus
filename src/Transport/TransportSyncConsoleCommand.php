<?php
declare(strict_types=1);

namespace GDXbsv\PServiceBus\Transport;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'p-service-bus:transport:sync', description: 'Make sure all necessary resources for your transports are created and configured.')]
class TransportSyncConsoleCommand extends Command
{
    /**
     * @param TransportSynchronisation[] $transportSynchronisations
     */
    public function __construct(private array $transportSynchronisations)
    {
        parent::__construct();
    }


    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln('Sync transport...');
        foreach ($this->transportSynchronisations as $transportSynchronisation) {
            $transportSynchronisation->sync();
        }
        $output->writeln('Sync done.');

        return 0;
    }
}
