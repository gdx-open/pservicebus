<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Transport\Sqs;

use GDXbsv\PServiceBus\Bus\Handling\Handle;
use GDXbsv\PServiceBus\Transport\Envelope;
use Ramsey\Uuid\Uuid;

/**
 * @internal
 */
final class SqsEnvelope
{
    /**
     * @param array<string, string|int|bool|null> $headers
     */
    public function __construct(
        private string $payload,
        private int $retries,
        private string $retriesTimeoutExpression,
        private int $timeoutSec,
        /** @var array<string, string|int|bool|null> */
        private array $headers,
    ) {
    }

    public static function ofEnvelope(Envelope $envelope): self
    {
        return new self(
            \json_encode($envelope->payload, JSON_THROW_ON_ERROR),
            $envelope->retries,
            $envelope->retriesTimeoutExpression,
            $envelope->timeoutSec,
            $envelope->headers
        );
    }

    /**
     * @param array{Body:string, MessageAttributes:array<string,array{StringValue: string, DataType: string}>, ...} $message
     */
    public static function ofSqs(array $message): self
    {
        $attributes = self::generateFromAttributes($message['MessageAttributes']);
        assert(isset($attributes['headers']) && is_string($attributes['headers']));
        /** @var array<string, bool|int|null|string> $headers */
        $headers = json_decode($attributes['headers'], true, 512, JSON_THROW_ON_ERROR);
        /** @var int|string $reties */
        $reties = $headers['retries'] ?? 0;
        /** @var int|string $timeoutSec */
        $timeoutSec = $headers['timeout_sec'] ?? 0;
        $retriesTimeoutExpression = $headers['retries_timeout_expression'] ?? (new Handle('empty'))->retriesTimeoutExpression;
        return new self($message['Body'], (int)$reties, (string)$retriesTimeoutExpression, (int)$timeoutSec, $headers);
    }

    public function toSqsArray(): array
    {
        $headers = $this->headers;
        $headers['retries'] = $this->retries;
        $headers['retries_timeout_expression'] = $this->retriesTimeoutExpression;
        $headers['timeout_sec'] = $this->timeoutSec;
        $messageAttributes = $this->generateAttributes(['headers' => json_encode($headers, JSON_THROW_ON_ERROR)]);
        if ($this->timeoutSec < 0 || $this->timeoutSec > 900) {
            throw new \Exception('Sorry but we support 0-900 sec timeout for SQS only;');
        }
        $message = [
            'MessageBody' => $this->payload,
            'Id' => Uuid::uuid7()->toString(),
            'DelaySeconds' => $this->timeoutSec,
        ];
        $message['MessageAttributes'] = $messageAttributes;

        return $message;
    }

    public function toEnvelope(): Envelope
    {
        /** @psalm-suppress MixedArgument we control what we receive. It must be an array */
        return new Envelope(\json_decode($this->payload, true), $this->retries, $this->retriesTimeoutExpression, $this->timeoutSec, $this->headers);
    }

    /**
     * @param iterable<string,bool|int|null|string> $attributes
     */
    private function generateAttributes(iterable $attributes): array
    {
        $messageAttributes = [];
        foreach ($attributes as $attributeName => $attributeValue) {
            $dataType = 'String';
            if (is_numeric($attributeValue)) {
                $dataType = 'Number';
            }
            if (is_bool($attributeValue)) {
                $dataType = 'Number.Bool';
                $attributeValue = $attributeValue ? 1:0;
            }
            $messageAttributes[$attributeName] = [
                'DataType' => $dataType,
                'StringValue' => $attributeValue,
            ];
        }

        return $messageAttributes;
    }

    /**
     * @param iterable<string,array{StringValue: string, DataType: string}> $messageAttributes
     * @return array<string, bool|int|null|string>
     */
    private static function generateFromAttributes(iterable $messageAttributes): array
    {
        $attributes = [];
        foreach ($messageAttributes as $attributeName => ['StringValue' => $val, 'DataType' => $type]) {
            $attributes[$attributeName] = match ($type) {
                'String' => $val,
                'Number' => (int)$val,
                'Number.Bool' => (bool)$val,
            };
        }

        return $attributes;
    }
}
