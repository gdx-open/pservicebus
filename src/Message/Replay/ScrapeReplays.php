<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Message\Replay;

/**
 * @immutable
 * @psalm-immutable
 */
final class ScrapeReplays
{
    /**
     * @param list<class-string> $classNames
     * @return array<non-empty-string, ReplayInstruction>
     */
    public static function fromClasses(iterable $classNames): array
    {
        $replays = [];

        foreach ($classNames as $className) {
            $reflectionClass = new \ReflectionClass($className);
            foreach ($reflectionClass->getMethods() as $method) {
                $attributes = $method->getAttributes(Replay::class);
                if (count($attributes) === 0) {
                    continue;
                }

                foreach ($attributes as $attribute) {
                    $replayAttr = $attribute->newInstance();
                    $methodName = $method->getName();
                    $replays[$replayAttr->replayName] = new ReplayInstruction($className, $methodName);
                }
            }
        }

        return $replays;
    }
}
