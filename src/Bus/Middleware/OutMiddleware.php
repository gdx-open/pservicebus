<?php
declare(strict_types=1);

namespace GDXbsv\PServiceBus\Bus\Middleware;

use GDXbsv\PServiceBus\Message\Message;

interface OutMiddleware
{
    public function before(Message $message): void;
    public function after(): void;
}
