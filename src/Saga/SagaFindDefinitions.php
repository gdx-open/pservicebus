<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

interface SagaFindDefinitions
{
    /**
     * @return \Traversable<SagaFinderDefinition>
     */
    public function sagasForMessage(object $message): \Traversable;
    /**
     * @return \Traversable<SagaCreatorDefinition>
     */
    public function sagaCreatorsForMessage(object $message): \Traversable;
}
