<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus;

use GDXbsv\PServiceBus\Bus\Handling\OnlyOnce\OnlyOnceInitConsoleCommand;
use GDXbsv\PServiceBus\Doctrine\SagaDoctrineOutboxInitConsoleCommand;
use GDXbsv\PServiceBus\Transport\TransportSyncConsoleCommand;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\CommandNotFoundException;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(name: 'p-service-bus:init', description: 'Init everything for p-service-bus.')]
class InitConsoleCommand extends Command
{
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /** @var Application $application */
        $application = $this->getApplication();

        /** @psalm-suppress PossiblyNullArgument we now that this command has $defaultName */
        $command = $application->find(TransportSyncConsoleCommand::getDefaultName());
        $command->run(new ArrayInput([]), $output);

        /** @psalm-suppress PossiblyNullArgument we now that this command has $defaultName */
        $command = $application->find(OnlyOnceInitConsoleCommand::getDefaultName());
        $command->run(new ArrayInput([]), $output);

        try {
            /** @psalm-suppress PossiblyNullArgument we now that this command has $defaultName */
            $command = $application->find(SagaDoctrineOutboxInitConsoleCommand::getDefaultName());
            $command->run(new ArrayInput([]), $output);
        } catch (CommandNotFoundException) {
            // ignore if no command
        }


        return self::SUCCESS;
    }
}
