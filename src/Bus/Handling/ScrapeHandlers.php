<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Bus\Handling;

use GDXbsv\PServiceBus\Message\TimeSpan;

/**
 * @immutable
 * @psalm-immutable
 * @psalm-type MessageMap=array<class-string, list<MessageHandleInstruction>>
 */
final class ScrapeHandlers
{
    /**
     * @param list<class-string> $classNames
     * @return MessageMap
     */
    public static function fromClasses(iterable $classNames): array
    {
        $handlers = [];

        foreach ($classNames as $className) {
            $reflectionClass = new \ReflectionClass($className);
            foreach ($reflectionClass->getMethods() as $method) {
                $attributes = $method->getAttributes(Handle::class);
                if (count($attributes) === 0) {
                    continue;
                }
                $arguments = $method->getParameters();
                assert(isset($arguments[0]));
                /** @var \ReflectionNamedType $type */
                $type = $arguments[0]->getType();
                /** @var class-string $messageType */
                $messageType = $type->getName();
                $isStatic = $method->isStatic();
                $methodName = $method->getName();

                foreach ($attributes as $attribute) {
                    $handleAttr = $attribute->newInstance();
                    $handlers[$messageType][] = new MessageHandleInstruction(
                        $messageType,
                        $handleAttr->transportName,
                        $handleAttr->retries,
                        $handleAttr->timeoutSec !== null ? TimeSpan::fromSeconds($handleAttr->timeoutSec) : null,
                        $className,
                        $methodName,
                        $isStatic,
                        $handleAttr->retriesTimeoutExpression,
                    );
                }
            }
        }

        return $handlers;
    }
}
