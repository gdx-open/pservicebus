<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Bus\Handling;

/**
 * @immutable
 * @psalm-immutable
 */
#[\Attribute(\Attribute::TARGET_FUNCTION | \Attribute::TARGET_METHOD)]
final readonly class Handle
{
    /**
     * @param non-empty-string $transportName
     */
    public function __construct(
        /** @var non-empty-string */
        public string $transportName,
        public ?int $retries = null,
        public ?int $timeoutSec = null,
        /**
         * Allow to set expression to calculate retries timeout.
         * We pass inside variable `retries_count`
         * Usage see https://symfony.com/doc/current/reference/formats/expression_language.html
         */
        public string $retriesTimeoutExpression = '60 * (2 ** retries_count)',
    ) {
    }
}
