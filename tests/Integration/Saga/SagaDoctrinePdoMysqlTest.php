<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBusTests\Integration\Saga;

class SagaDoctrinePdoMysqlTest extends SagaDoctrine
{
    public function connectionConfig(): array
    {
        return [
//            'url' => 'mysql://p-service-bus:p-service-bus_pass@mysql/p-service-bus',
            'driver' => 'pdo_mysql',
            'dbname' => 'p-service-bus',
            'user' => 'p-service-bus',
            'password' => 'p-service-bus_pass',
            'host' => 'mysql',
//            'path' => getcwd() . '/db.sqlite',
        ];
    }
}
