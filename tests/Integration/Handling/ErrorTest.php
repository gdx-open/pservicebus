<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBusTests\Integration\Handling;

use GDXbsv\PServiceBus\Transport\Envelope;
use GDXbsv\PServiceBusTests\Integration\IntegrationTestCase;
use JetBrains\PhpStorm\Immutable;

/**
 * @internal
 * @immutable
 * @psalm-immutable
 */
#[Immutable]
final class ErrorTest extends IntegrationTestCase
{
    public function testBrokenMessages()
    {
        $inMemmory = $this->inMemTransport;
        $sending = $inMemmory->sending();
        self::expectExceptionMessage('assert(array_key_exists(\'name\', $headers))');
        $sending->send(new Envelope([], 0,'0', 0, []));
        $sending->finish(null);
    }
}
