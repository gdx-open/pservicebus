<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

use GDXbsv\PServiceBus\Bus;
use GDXbsv\PServiceBus\Bus\CoroutineBus;
use GDXbsv\PServiceBus\Message\CommandOptions;
use GDXbsv\PServiceBus\Message\EventOptions;
use GDXbsv\PServiceBus\Message\Message;
use GDXbsv\PServiceBus\Message\MessageOptions;
use GDXbsv\PServiceBus\Message\TimeSpan;

/**
 * @psalm-suppress MissingImmutableAnnotation Will remove it in the next version
 */
final class SagaContext
{
    /** @var list<Message<EventOptions>> */
    private array $published = [];

    public function __construct(
        public MessageOptions $messageOptions,
        private Saga $saga,
    ) {
    }

    public function timeout(object $event, TimeSpan $timeSpan): void
    {
        /** @psalm-suppress InvalidPropertyAssignmentValue psalm does not detect correct type */
        $this->published[] = new Message(
            $event,
            EventOptions::record()->withHeader(
                'playhead',
                $this->getPlayhead()
            )->withTimeout($timeSpan)
        );
        $this->upPlayhead();
    }

    public function publish(object $message, ?EventOptions $eventOptions = null): void
    {
        $options = $eventOptions ?? EventOptions::record();
        $options = $options->withHeader('playhead', $this->getPlayhead());
        /** @psalm-suppress InvalidPropertyAssignmentValue psalm does not see it is always a correct EventOption */
        $this->published[] = new Message($message, $options);
        $this->upPlayhead();
    }

    private function upPlayhead(): void
    {
        $closure = \Closure::bind(
            closure: static function (Saga $saga): void {
                /** @psalm-suppress InaccessibleProperty we can inside closure */
                ++$saga->_playhead;
            },
            newThis:  null,
            newScope: $this->saga::class
        );

        /**
         * @psalm-suppress PossiblyNullFunctionCall
         */
        $closure($this->saga);
    }

    /**
     * @return positive-int
     */
    private function getPlayhead(): int
    {
        $closure = \Closure::bind(
            closure: static function (Saga $saga): int {
                /** @psalm-suppress InaccessibleProperty we can inside closure */
                return $saga->_playhead;
            },
            newThis:  null,
            newScope: $this->saga::class
        );

        /**
         * @psalm-suppress PossiblyNullFunctionCall
         * @psalm-suppress MixedReturnStatement
         */
        return $closure($this->saga);
    }
}
