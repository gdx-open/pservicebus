<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

final class CorrelatedSagaPropertyMapper
{
    /**
     * @param class-string<Saga> $sagaType
     */
    public function __construct(private SagaMessageFindingConfiguration $sagaMessageFindingConfiguration, private string $sagaType, private \ReflectionProperty $sagaProperty)
    {
    }

    /**
     * @param \Closure(object):\GDXbsv\PServiceBus\Id<Saga> $messageProperty
     */
    public function toMessage(\Closure $messageProperty): self
    {
        $this->sagaMessageFindingConfiguration->configureFindMapping($this->sagaType, $this->sagaProperty, $messageProperty);

        return $this;
    }
}
