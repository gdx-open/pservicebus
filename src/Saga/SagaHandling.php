<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

use GDXbsv\PServiceBus\Message\EventOptions;
use GDXbsv\PServiceBus\Message\Message;

final class SagaHandling
{
    public function __construct(private SagaPersistence $sagaPersistence,)
    {
    }

    /**
     * @param Message<EventOptions> $message
     * @return list<Message<EventOptions>>
     */
    public function handle(Message $message): array
    {
        try {
            assert(isset($message->options->headers['saga']));
            /** @var class-string<Saga> $sagaType */
            $sagaType = $message->options->headers['saga'];
            $saga = $this->sagaPersistence->retrieveSaga($message, $sagaType);
            if (!$saga) {
                return [];
            }
            $messages = $this->execute($message, $saga);
            $this->sagaPersistence->saveSaga($saga, $messages);
            return $messages;
        } catch (\Throwable $throwable) {
            if (isset($saga)) {
                $this->sagaPersistence->cleanSaga($saga);
            }
            throw $throwable;
        }
    }

    /**
     * @param Message<EventOptions> $message
     * @return list<Message<EventOptions>>
     */
    protected function execute(Message $message, Saga $saga): array
    {
        $messagesOut = [];
        $messagePayload = $message->payload;
        $sagaReflection = new \ReflectionClass($saga);
        foreach (
            $sagaReflection->getMethods(
                \ReflectionMethod::IS_STATIC | \ReflectionMethod::IS_PUBLIC
            ) as $reflectionMethod
        ) {
            $parameters = $reflectionMethod->getParameters();
            if (count($parameters) !== 2) {
                continue;
            }
            [$messageParameter, ,] = $parameters;
            /** @var \ReflectionNamedType|null $messageType */
            $messageType = $messageParameter->getType();
            if ($messageType && $messageType->getName() !== $messagePayload::class) {
                continue;
            }
            $sagaContext = new SagaContext($message->options, $saga);
            $reflectionMethod->invokeArgs(
                $saga,
                [
                    $messagePayload,
                    $sagaContext,
                ]
            );

            $messagesOut[] = $this->extractMessages($sagaContext);
        }

        return array_merge([], ...$messagesOut);
    }

    /**
     * @return list<Message<EventOptions>>
     */
    private function extractMessages(SagaContext $sagaContext): array
    {
        $closure = \Closure::bind(
            /** @return  list<Message<EventOptions>> */
            closure: static function (SagaContext $sagaContext): array {
                /** @psalm-suppress InaccessibleProperty we can inside closure */
                return $sagaContext->published;
            },
            newThis:  null,
            newScope: SagaContext::class
        );

        /**
         * @psalm-suppress PossiblyNullFunctionCall
         * @psalm-suppress MixedReturnStatement
         */
        return $closure($sagaContext);
    }
}
