<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Doctrine;

use Doctrine\DBAL\Connection;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(name: 'p-service-bus:saga:doctrine:outbox:init', description: 'Init table.')]
class SagaDoctrineOutboxInitConsoleCommand extends Command
{
    public function __construct(private Connection $connection, private string $tableOutbox)
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $io->writeln('Start');
        $this->connection->executeStatement(
            "CREATE TABLE IF NOT EXISTS {$this->tableOutbox} (message_id VARCHAR(36) PRIMARY KEY, message TEXT);"
        );

        $io->writeln('Succeed');
        return self::SUCCESS;
    }
}
