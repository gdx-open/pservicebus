<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Transport\Sns;

use GDXbsv\PServiceBus\Bus\Handling\Handle;
use GDXbsv\PServiceBus\Transport\Envelope;

/**
 * @internal
 */
final class SnsEnvelope
{
    /**
     * @param array<string, string|int|bool|null> $headers
     */
    public function __construct(
        private string $payload,
        private int $retries,
        private string $retriesTimeoutExpression,
        private int $timeoutSec,
        /** @var array<string, string|int|bool|null> */
        private array $headers,
    ) {
    }

    public static function ofEnvelope(Envelope $envelope): self
    {
        return new self(
            \json_encode($envelope->payload, JSON_THROW_ON_ERROR),
            $envelope->retries,
            $envelope->retriesTimeoutExpression,
            $envelope->timeoutSec,
            $envelope->headers
        );
    }

    public static function ofSqsRaw(array $messageRaw): self
    {
        assert(isset($messageRaw['Body']));
        assert(is_string($messageRaw['Body']));
        /** @var array{Type:string, Message: string, MessageId:string, TopicArn:string, Timestamp:string, MessageAttributes: array<string,array{Value: string, Type: string}>} $body */
        $body = json_decode($messageRaw['Body'], true, 512, JSON_THROW_ON_ERROR);
        /** @var array{payload:string, headers: array<string, bool|int|null|string>} $message */
        $message = json_decode($body['Message'], true, 512, JSON_THROW_ON_ERROR);
        $headers = $message['headers'];
        /** @var int|string $reties */
        $reties = $headers['retries'] ?? 0;
        $retriesTimeoutExpression = $headers['retries_timeout_expression'] ?? (new Handle('empty'))->retriesTimeoutExpression;
        /** @var int|string $timeoutSec */
        $timeoutSec = $headers['timeout_sec'] ?? 0;
        return new self(
            $message['payload'],
            (int)$reties,
            (string)$retriesTimeoutExpression,
            (int)$timeoutSec,
            $headers
        );
    }

    public function toSnsArray(string $topicArn): array
    {
        $headers = $this->headers;
        $headers['retries'] = $this->retries;
        $headers['retries_timeout_expression'] = $this->retriesTimeoutExpression;
        $headers['timeout_sec'] = $this->timeoutSec;
        assert(isset($headers['name']));
        $messageAttributes = $this->generateAttributes(
            [
                'headers' => 'moved_to_body',
                'name' => $headers['name'],
            ]
        );

        $publish = [
            'Message' => json_encode([
                                         'payload' => $this->payload,
                                         'headers' => $headers
                                     ], JSON_THROW_ON_ERROR),
            'TopicArn' => $topicArn,
        ];

        if (0 !== count($messageAttributes)) {
            $publish['MessageAttributes'] = $messageAttributes;
        }

        return $publish;
    }

    public function toEnvelope(): Envelope
    {
        /** @psalm-suppress MixedArgument we control what we receive. It must be an array */
        return new Envelope(
            \json_decode($this->payload, true, 512, JSON_THROW_ON_ERROR),
            $this->retries,
            $this->retriesTimeoutExpression,
            $this->timeoutSec,
            $this->headers
        );
    }

    /**
     * @param iterable<string,string|int|bool|null> $attributes
     */
    private function generateAttributes(iterable $attributes): array
    {
        $messageAttributes = [];
        foreach ($attributes as $attributeName => $attributeValue) {
            $dataType = 'String';
            if (is_numeric($attributeValue)) {
                $dataType = 'Number';
            }
            $messageAttributes[$attributeName] = [
                'DataType' => $dataType,
                'StringValue' => $attributeValue,
            ];
        }

        return $messageAttributes;
    }

    /**
     * @param iterable<string,array{Value: string, Type: string}> $messageAttributes
     * @return array<string, bool|int|null|string>
     */
    private static function generateFromAttributes(iterable $messageAttributes): array
    {
        $attributes = [];
        foreach ($messageAttributes as $attributeName => ['Value' => $val, 'Type' => $type]) {
            $attributes[$attributeName] = match ($type) {
                'String' => $val,
                'Number' => (int)$val,
            };
        }

        return $attributes;
    }
}
