<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

use GDXbsv\PServiceBus\Message\EventOptions;
use GDXbsv\PServiceBus\Message\Message;

interface SagaPersistence
{
    /**
     * @param Message<EventOptions> $message
     * @param class-string<Saga> $sagaType
     */
    public function retrieveSaga(Message $message, string $sagaType): ?Saga;

    /**
     * @param list<Message<EventOptions>> $messages
     */
    public function saveSaga(Saga $saga, array $messages): void;

    public function cleanSaga(Saga $saga): void;
}
