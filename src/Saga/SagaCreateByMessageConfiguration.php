<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

interface SagaCreateByMessageConfiguration
{
    /**
     * @param class-string<Saga> $sagaType
     * @param \Closure(object, MessageSagaContext):?Saga $sagaCreator
     */
    public function configureCreateMapping(
        string $sagaType,
        \Closure $sagaCreator
    ): void;
}
