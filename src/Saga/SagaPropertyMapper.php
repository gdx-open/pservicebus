<?php declare(strict_types=1);

namespace GDXbsv\PServiceBus\Saga;

final class SagaPropertyMapper
{
    /**
     * @param class-string<Saga> $sagaType
     */
    public function __construct(private string $sagaType, private SagaMessageFindingConfiguration $sagaMessageFindingConfiguration)
    {
    }

    public function mapSaga(\ReflectionProperty $sagaProperty): CorrelatedSagaPropertyMapper
    {
        return new CorrelatedSagaPropertyMapper($this->sagaMessageFindingConfiguration, $this->sagaType, $sagaProperty);
    }
}
