<?php

declare(strict_types=1);

namespace GDXbsv\PServiceBusTests\Integration\Saga;

class SagaDoctrineSqliteTest extends SagaDoctrine
{
    public function connectionConfig(): array
    {
        return [
            'driver' => 'pdo_sqlite',
            'path' => getcwd() . '/db.sqlite',
        ];
    }
}
